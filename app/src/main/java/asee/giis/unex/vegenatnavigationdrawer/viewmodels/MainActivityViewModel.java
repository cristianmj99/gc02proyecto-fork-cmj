package asee.giis.unex.vegenatnavigationdrawer.viewmodels;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.ViewModel;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.ProductWithQuantity;
import asee.giis.unex.vegenatnavigationdrawer.repository.VegenatRepository;

/**
 * ViewModel para la vista de MainActivity
 **/
public class MainActivityViewModel extends ViewModel {

    //Repositorio para el acceso a datos
    private final VegenatRepository vegenatRepository;

    public MainActivityViewModel (VegenatRepository userRepositoryP) {
        vegenatRepository = userRepositoryP; //asignamos el repo porque va a ser único
    }

    public void deleteUser (long id) {
        vegenatRepository.deleteUser(id);
    }

    public void deleteProductShoppingItem (ProductWithQuantity productWithQuantity) {
        vegenatRepository.deleteProductShoppingList(productWithQuantity);
    }

    /**
     * Insertamos una reclamacion
     **/
    public void insertClaim (Context context, long id_user, long id_order) {
        vegenatRepository.insertClaim(context, id_order, id_user);
    }
}
