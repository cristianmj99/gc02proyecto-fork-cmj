package asee.giis.unex.vegenatnavigationdrawer.repository.model.local;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "claims", indices = {@Index(value = {"id_claim"}, unique = true)})
public class Claim {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_claim")
    private long id_claim;

    @ColumnInfo(name = "id_user")
    private long id_user;

    @ColumnInfo(name = "id_order")
    private long id_order;

    //Constructor parametrizado
    public Claim (long id_order, long id_user) {
        this.id_order = id_order;
        this.id_user = id_user;
    }

    //Getters y setters
    public long getId_claim() {
        return id_claim;
    }

    public void setId_claim(long id_claim) {
        this.id_claim = id_claim;
    }

    public long getId_order() {
        return id_order;
    }

    public void setId_order(long id_order) {
        this.id_order = id_order;
    }

    public long getId_user() {
        return id_user;
    }

    public void setId_user(long id_user) {
        this.id_user = id_user;
    }
}
