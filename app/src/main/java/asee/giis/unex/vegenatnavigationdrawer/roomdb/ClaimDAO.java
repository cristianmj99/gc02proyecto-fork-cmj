package asee.giis.unex.vegenatnavigationdrawer.roomdb;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Claim;

@Dao
public interface ClaimDAO {

    /**
     * Insertar una reclamación en la BD
     **/
    @Insert
    void insertClaim (Claim claim);

    /**
     * Obtener las reclamaciones de un usuario
     **/
    @Query("SELECT * FROM claims WHERE id_user = :id_user")
    List<Claim> getClaimsFromUser(long id_user);
}
