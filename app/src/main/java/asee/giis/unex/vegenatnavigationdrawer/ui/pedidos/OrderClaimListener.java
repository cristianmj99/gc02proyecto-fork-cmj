package asee.giis.unex.vegenatnavigationdrawer.ui.pedidos;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Order;
public interface OrderClaimListener {
    void onClaimInsert(Order order);
}
