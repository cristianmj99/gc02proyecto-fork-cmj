package asee.giis.unex.vegenatnavigationdrawer;

import androidx.lifecycle.LiveData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import asee.giis.unex.vegenatnavigationdrawer.api.ProductInterfaceAPI;
import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Product;
import asee.giis.unex.vegenatnavigationdrawer.repository.model.network.ProductNetworkDataSource;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ServiceAPITest {

    @Test
    public void getAPIProductTest() throws JsonProcessingException {
        List<Product> products = loadProductsInList();

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("https://raw.githubusercontent.com/").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only listProducts request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(products));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        ProductInterfaceAPI service = retrofit.create(ProductInterfaceAPI.class);

        //Step6: we create the call to get centers
        Call<List<Product>> call = service.listProducts();
        // and we execute the call
        Response<List<Product>> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Product> productsResponse = response.body();

        assertFalse(productsResponse.isEmpty());
        assertTrue(productsResponse.size() == 6);

        //Check objects lists
        assertEquals(productsResponse.get(0).getId(), products.get(0).getId());
        assertEquals(productsResponse.get(0).getTitle(), products.get(0).getTitle());
        assertEquals(productsResponse.get(0).getDescription(), products.get(0).getDescription());
        assertEquals(productsResponse.get(0).getPrice(), products.get(0).getPrice());
        //Step9: Finish web server
        try {
            mockWebServer.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Product> loadProductsInList() {
        List<Product> allProducts = new ArrayList<Product>();
        allProducts.add(new Product(1, "Ajo copos", "Producido a partir de ajo seleccionado, pelado, cortado y deshidratado.", 20.5F, "https://www.vegenat.es/wp-content/uploads/2019/03/ajo-copos-1.png"));
        allProducts.add(new Product(2, "AJO MINCED", "Ajo Minced. Producido a partir de ajo seleccionado, pelado, cortado y deshidratado", 25.7F, "https://www.vegenat.es/wp-content/uploads/2019/03/ajo-corte-especial-1.png"));
        allProducts.add(new Product(3, "Ajo Polvo", "Ajo polvo Agrotécnica. Producido a partir de ajo seleccionado, pelado, cortado y deshidratado.", 18.0F, "https://www.vegenat.es/wp-content/uploads/2019/03/ajo-polvo-1.png"));
        allProducts.add(new Product(4, "Cebolla Eco", "Cebolla ecológica Agrotécnica. Cebolla procedente de agricultura ecológica, cortada y deshidratada.", 40.0F, "https://www.vegenat.es/wp-content/uploads/2019/03/cebolla-eco-1.png"));
        allProducts.add(new Product(5, "Espinaca", "Espinaca deshidratada Agrotécnica. Producido a partir de espinaca lavada, deshidratada y molturada.", 30.0F, "https://www.vegenat.es/wp-content/uploads/2019/03/espinaca-1.png"));
        allProducts.add(new Product(6, "Pimiento Verde", "Pimiento Verde Deshidratado Agrotécnica. Producido a partir de pimiento verde seleccionado, lavado, descorazonado, cortado y deshidratado.", 22.0F, "https://www.vegenat.es/wp-content/uploads/2019/03/pimiento-verde-1.png"));
        return allProducts;
    }

}
