package asee.giis.unex.vegenatnavigationdrawer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Order;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class OrderUnitTest {

    private Order order;

    @Before
    public void setUp() throws Exception {
        order = new Order(2, "27/11/2021", 22.5F, "C/ Gil Cordero, 6, 4º A");
        order.setId_order(1);
    }

    @Test
    public void orderShouldHaveIdOrder() {
        assertNotNull(order.getId_order());
        assertEquals(order.getId_order(), 1);
    }

    @Test
    public void idOrderShouldBeChanged() {
        Order orderTest = new Order(3, "06/10/2021", 33.3F, "Avda. Alemania, 23, 3º B");
        orderTest.setId_order(4);
        assertNotNull(orderTest.getId_order());
        assertEquals(orderTest.getId_order(), 4);
    }

    @Test
    public void orderShouldHaveIdUser() {
        assertNotNull(order.getId_user());
        assertEquals(order.getId_user(), 2);
    }

    @Test
    public void idUserShouldBeChanged() {
        Order orderTest = new Order(3, "06/10/2021", 33.3F, "Avda. Alemania, 23, 3º B");
        orderTest.setId_user(9);
        assertNotNull(orderTest.getId_user());
        assertEquals(orderTest.getId_user(), 9);
    }

    @Test
    public void orderShouldHaveDate() {
        assertNotNull(order.getDate());
        assertEquals(order.getDate(), "27/11/2021");
    }

    @Test
    public void dateShouldBeChanged() {
        Order orderTest = new Order(3, "06/10/2021", 33.3F, "Avda. Alemania, 23, 3º B");
        orderTest.setDate("05/01/2021");
        assertNotNull(orderTest.getDate());
        assertEquals(orderTest.getDate(), "05/01/2021");
    }

    @Test
    public void orderShouldHaveTotalPrice() {
        assertNotNull(order.getTotal_price());
        assertEquals(order.getTotal_price(), 22.5F, 0.0F);
    }

    @Test
    public void totalPriceShouldBeChanged() {
        Order orderTest = new Order(3, "06/10/2021", 33.3F, "Avda. Alemania, 23, 3º B");
        orderTest.setTotal_price(40.9F);
        assertNotNull(orderTest.getTotal_price());
        assertEquals(orderTest.getTotal_price(), 40.9F, 0.0F);
    }

    @Test
    public void orderShouldHaveDeliveryAddress() {
        assertNotNull(order.getDelivery_address());
        assertEquals(order.getDelivery_address(), "C/ Gil Cordero, 6, 4º A");
    }

    @Test
    public void deliveryAddressShouldBeChanged() {
        Order orderTest = new Order(3, "06/10/2021", 33.3F, "Avda. Alemania, 23, 3º B");
        orderTest.setDelivery_address("C/ Argentina, 11, 2A");
        assertNotNull(orderTest.getDelivery_address());
        assertEquals(orderTest.getDelivery_address(), "C/ Argentina, 11, 2A");
    }

    @After
    public void tearDown() throws Exception {
        order = null;
    }
}