package asee.giis.unex.vegenatnavigationdrawer;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Claim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ClaimUnitTest {

    private Claim c;

    @Before
    public void setUp() throws Exception {
        c = new Claim(2, 3);
        c.setId_claim(1);
    }

    @Test
    public void shouldHaveIdClaim() {
        assertNotNull(c.getId_claim());
        assertEquals(c.getId_claim(), 1);
    }

    @Test
    public void idClaimShouldBeChanged() {
        Claim claimTest = new Claim(5, 6);
        claimTest.setId_claim(8);
        assertNotNull(claimTest.getId_claim());
        assertEquals(claimTest.getId_claim(), 8);
    }

    @Test
    public void shouldHaveIdOrder() {
        assertNotNull(c.getId_order());
        assertEquals(c.getId_order(), 2);
    }

    @Test
    public void idOrderShouldBeChanged() {
        Claim claimTest = new Claim(5, 6);
        claimTest.setId_order(9);
        assertNotNull(claimTest.getId_order());
        assertEquals(claimTest.getId_order(), 9);
    }

    @Test
    public void shouldHaveIdUser() {
        assertNotNull(c.getId_user());
        assertEquals(c.getId_user(), 3);
    }

    @Test
    public void idUserShouldBeChanged() {
        Claim claimTest = new Claim(5, 6);
        claimTest.setId_user(11);
        assertNotNull(claimTest.getId_user());
        assertEquals(claimTest.getId_user(), 11);
    }

    @After
    public void tearDown() throws Exception {
        c = null;
    }
}