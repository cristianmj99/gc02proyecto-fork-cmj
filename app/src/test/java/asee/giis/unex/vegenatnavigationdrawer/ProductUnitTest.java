package asee.giis.unex.vegenatnavigationdrawer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ProductUnitTest  {

    private Product p;

    @Before
    public void setUp() throws Exception {
        p = new Product(10, "Cebolla", "Cebolla ecológica", 6.7F, "www.example.com/cebolla");
    }

    @Test
    public void productShouldHaveId() {
        assertNotNull(p.getId());
        assertEquals(p.getId(), 10);
    }

    @Test
    public void idShouldBeChanged() {
        Product pTest = new Product(12, "Ajo", "Ajo ecológico", 4.9F, "www.example.com/ajo");
        pTest.setId(15);
        assertNotNull(pTest.getId());
        assertEquals(pTest.getId(), 15);
    }

    @Test
    public void productShouldHaveTitle() {
        assertNotNull(p.getTitle());
        assertEquals(p.getTitle(), "Cebolla");
    }

    @Test
    public void titleShouldBeChanged() {
        Product pTest = new Product(12, "Ajo", "Ajo ecológico", 4.9F, "www.example.com/ajo");
        pTest.setTitle("Ajo ecológico nueva receta");
        assertNotNull(pTest.getTitle());
        assertEquals(pTest.getTitle(), "Ajo ecológico nueva receta");
    }

    @Test
    public void productShouldHaveDescription() {
        assertNotNull(p.getDescription());
        assertEquals(p.getDescription(), "Cebolla ecológica");
    }

    @Test
    public void descriptionShouldBeChanged() {
        Product pTest = new Product(12, "Ajo", "Ajo ecológico", 4.9F, "www.example.com/ajo");
        pTest.setDescription("Nuevo ajo ecológico en Vegenat");
        assertNotNull(pTest.getDescription());
        assertEquals(pTest.getDescription(), "Nuevo ajo ecológico en Vegenat");
    }

    @Test
    public void productShouldHavePrice() {
        assertNotNull(p.getPrice());
        assertEquals(p.getPrice(), 6.7F, 0.00F);
    }

    @Test
    public void priceShouldBeChanged() {
        Product pTest = new Product(12, "Ajo", "Ajo ecológico", 4.9F, "www.example.com/ajo");
        pTest.setPrice(5.7F);
        assertNotNull(pTest.getPrice());
        assertEquals(pTest.getPrice(), 5.7F, 0.0F);
    }

    @Test
    public void productShouldHaveImageLink() {
        assertNotNull(p.getImagelink());
        assertEquals(p.getImagelink(), "www.example.com/cebolla");
    }

    @Test
    public void imageLinkShouldBeChanged() {
        Product pTest = new Product(12, "Ajo", "Ajo ecológico", 4.9F, "www.example.com/ajo");
        pTest.setImagelink("www.example.com/Vegenat/ajo");
        assertNotNull(pTest.getImagelink());
        assertEquals(pTest.getImagelink(), "www.example.com/Vegenat/ajo");
    }

    @After
    public void tearDown() throws Exception {
        p = null;
    }
}