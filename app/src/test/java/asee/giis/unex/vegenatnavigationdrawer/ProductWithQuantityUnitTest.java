package asee.giis.unex.vegenatnavigationdrawer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.ProductWithQuantity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class ProductWithQuantityUnitTest {

    private ProductWithQuantity pwq;

    @Before
    public void setUp() throws Exception {
        pwq = new ProductWithQuantity(4, "Ajo", 4, 11.2f);
        pwq.setIdItemLista(1);
    }

    @Test
    public void productWithQuantityShouldHaveIdItemLista() {
        assertNotNull(pwq.getIdItemLista());
        assertEquals(pwq.getIdItemLista(), 1);
    }

    @Test
    public void idItemListaShouldntBeNull() {
        ProductWithQuantity pwqTest = new ProductWithQuantity(0, "", 0, 0.0f);
        pwqTest.setIdItemLista(5);
        assertNotNull(pwqTest.getIdItemLista());
        assertEquals(pwqTest.getIdItemLista(), 5);
    }

    @Test
    public void productWithQuantityShouldHaveIdUser() {
        assertNotNull(pwq.getId_user());
        assertEquals(pwq.getId_user(), 4);
    }

    @Test
    public void idItemListaShouldBeChanged() {
        ProductWithQuantity pwqTest = new ProductWithQuantity(21, "", 0, 0.0f);
        pwqTest.setId_user(20);
        assertNotNull(pwqTest.getId_user());
        assertEquals(pwqTest.getId_user(), 20);
    }

    @Test
    public void productWithQuantityShouldHavePrice() {
        assertNotNull(pwq.getPrice());
        assertEquals(pwq.getPrice(), 11.2f, 0.02f);
    }

    @Test
    public void priceShouldBeChanged() {
        ProductWithQuantity pwqTest = new ProductWithQuantity(0, "", 0, 2.30f);
        pwqTest.setPrice(2.40f);
        assertNotNull(pwqTest.getPrice());
        assertEquals(pwqTest.getPrice(), 2.40f, 0.02f);
    }

    @Test
    public void productWithQuantityShouldHavePriceProductName() {
        assertNotNull(pwq.getProduct_name());
        assertEquals(pwq.getProduct_name(), "Ajo");
    }

    @Test
    public void productNameShouldntBeNull() {
        ProductWithQuantity pwqTest = new ProductWithQuantity(0, null, 0, 2.30f);
        pwqTest.setProduct_name("Cebolla ecológica");
        assertNotNull(pwqTest.getProduct_name());
        assertEquals(pwqTest.getProduct_name(), "Cebolla ecológica");
    }

    @Test
    public void productWithQuantityShouldHaveQuantity() {
        assertNotNull(pwq.getQuantity());
        assertNotEquals(pwq.getQuantity(), 0);
        assertEquals(pwq.getQuantity(), 4);
    }

    @Test
    public void quantityShouldntBeZero() {
        ProductWithQuantity pwqTest = new ProductWithQuantity(0, null, 0, 0.0f);
        pwqTest.setQuantity(7);
        assertNotEquals(pwqTest.getQuantity(), 0);
        assertEquals(pwqTest.getQuantity(), 7);
    }

    @After
    public void tearDown () throws Exception {
        pwq = null;
    }
}