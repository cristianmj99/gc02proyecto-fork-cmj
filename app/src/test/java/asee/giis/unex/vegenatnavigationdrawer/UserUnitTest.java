package asee.giis.unex.vegenatnavigationdrawer;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class UserUnitTest  {

    private User user1, user2;

    @Before
    public void setUp() throws Exception {
        user1 = new User("CristianMJ99", "cristianmj99@hotmail.com", "Hola12345!", "C/ Gil Cordero, 27, 3º A");
        user1.setId(50);
        user2 = new User("CristianRoman18", "cromanro@alumnos.unex.es", "adios09!ABC", null);
        user2.setId(51);
    }

    @Test
    public void userShouldBeHaveId() {
        assertNotNull(user1.getId());
        assertNotNull(user2.getId());
        assertEquals(user1.getId(), 50);
        assertEquals(user2.getId(), 51);
    }

    @Test
    public void idUserShouldBeChanged() {
        User userTest = new User("", "", "", "");
        userTest.setId(200);
        assertNotNull(userTest.getId());
        assertEquals(userTest.getId(), 200);
    }

    @Test
    public void userShouldBeHaveUsername() {
        assertNotNull(user1.getUsername());
        assertNotNull(user2.getUsername());
        assertEquals(user1.getUsername(), "CristianMJ99");
        assertEquals(user2.getUsername(), "CristianRoman18");
    }

    @Test
    public void usernameShouldBeChanged() {
        User userTest = new User(null, "", "", "");
        userTest.setUsername("CristianRR18");
        assertNotNull(userTest.getUsername());
        assertEquals(userTest.getUsername(), "CristianRR18");
    }

    @Test
    public void userShouldBeHaveEmail() {
        assertNotNull(user1.getEmail());
        assertNotNull(user2.getEmail());
        assertEquals(user1.getEmail(), "cristianmj99@hotmail.com");
        assertEquals(user2.getEmail(), "cromanro@alumnos.unex.es");
    }

    @Test
    public void emailShouldBeChanged() {
        User userTest = new User("", null, "", "");
        userTest.setEmail("crmartinj@alumnos.unex.es");
        assertNotNull(userTest.getEmail());
        assertEquals(userTest.getEmail(), "crmartinj@alumnos.unex.es");
    }

    @Test
    public void userShouldHavePassword() {
        assertNotNull(user1.getPassword());
        assertNotNull(user2.getPassword());
        assertEquals(user1.getPassword(), "Hola12345!");
        assertEquals(user2.getPassword(), "adios09!ABC");
    }

    @Test
    public void passwordShouldBeChanged() {
        User userTest = new User("", "", null, "");
        userTest.setPassword("");
        assertNotNull(userTest.getPassword());
        assertEquals(userTest.getPassword(), "");
    }

    @Test
    public void usersShouldHaveNullAndNotNullAddress() {
        assertNotNull(user1.getAddress());
        assertNull(user2.getAddress());
        assertEquals(user1.getAddress(), "C/ Gil Cordero, 27, 3º A");
    }

    @Test
    public void testSetAddress() {
        User userTest = new User("", "", "", "");
        userTest.setAddress("Avda. Alemania, 5, 4º B");
        assertNotNull(userTest.getAddress());
        assertEquals(userTest.getAddress(), "Avda. Alemania, 5, 4º B");
    }

    @After
    public void tearDown() throws Exception {
        user1 = null;
        user2 = null;
    }

}