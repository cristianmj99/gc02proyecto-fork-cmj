package asee.giis.unex.vegenatnavigationdrawer.uiTest;


import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;


import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import asee.giis.unex.vegenatnavigationdrawer.MainActivity;
import asee.giis.unex.vegenatnavigationdrawer.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.contrib.DrawerMatchers.isOpen;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class RegisterUserUITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void registerUserTest() {

        //Open user menu
        ViewInteraction menu = onView(allOf(withId(R.id.empty)));
        menu.perform(click());

        onView(withText("Registrar usuario")).perform(click());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatEditText1 = onView(allOf(withId(R.id.username)));
        appCompatEditText1.perform(replaceText("CristianMJ6999"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(allOf(withId(R.id.email)));
        appCompatEditText2.perform(replaceText("cristianmj123456@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(allOf(withId(R.id.address)));
        appCompatEditText3.perform(replaceText("Avda. Alemania, 21, 4ºB"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(allOf(withId(R.id.password)));
        appCompatEditText4.perform(replaceText("hola!12345"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(allOf(withId(R.id.confirmpassword)));
        appCompatEditText5.perform(replaceText("hola!12345"), closeSoftKeyboard());

        //Try to register with incorrect password (don't match regexp)
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.registrarse), withText("Registrar usuario")));
        appCompatButton.perform(click());

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Try to register with correct password but password and confirmpassword don't match
        appCompatEditText4.perform(replaceText("Hola12345!"), closeSoftKeyboard());

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        appCompatButton.perform(click());

        //Correct password
        appCompatEditText5.perform(replaceText("Hola12345!"), closeSoftKeyboard());
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        appCompatButton.perform(click());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Open Drawer to click on navigation.
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))) // Left Drawer should be closed.
                .perform(DrawerActions.open()); // Open Drawer
        ViewInteraction username = onView(allOf(withId(R.id.user_nav_drawer),
                withText("Sesión iniciada\nUsuario logeado: CristianMJ6999"), isDisplayed()));
        username.check(matches(isDisplayed()));

        onView(withId(R.id.drawer_layout))
                .check(matches(isOpen(Gravity.LEFT)))
                .perform(DrawerActions.close()); // Close Drawer
        //Delete user to prevent errors in following tests
        menu.perform(click());
        onView(withText("Borrar usuario")).perform(click());

        //Delay to show products
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}