package asee.giis.unex.vegenatnavigationdrawer.uiTest;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import asee.giis.unex.vegenatnavigationdrawer.MainActivity;
import asee.giis.unex.vegenatnavigationdrawer.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MakeOrderUITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void makeOrderTest() {
        //Open user menu
        ViewInteraction menu = onView(allOf(withId(R.id.empty)));
        menu.perform(click());

        onView(withText("Registrar usuario")).perform(click());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatEditText1 = onView(allOf(withId(R.id.username)));
        appCompatEditText1.perform(replaceText("CristianMJ6999"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(allOf(withId(R.id.email)));
        appCompatEditText2.perform(replaceText("cristianmj123456@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(allOf(withId(R.id.address)));
        appCompatEditText3.perform(replaceText("Avda. Alemania, 21, 4ºB"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(allOf(withId(R.id.password)));
        appCompatEditText4.perform(replaceText("Hola12345!"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(allOf(withId(R.id.confirmpassword)));
        appCompatEditText5.perform(replaceText("Hola12345!"), closeSoftKeyboard());

        //Try to register with incorrect password (don't match regexp)
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.registrarse), withText("Registrar usuario")));
        appCompatButton.perform(click());

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Test clicking in some element
        ViewInteraction recyclerViewInterac = onView(
                allOf(withId(R.id.product_list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)));
        recyclerViewInterac.perform(actionOnItemAtPosition(2, click()));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Set quantity
        ViewInteraction quantity = onView(allOf(withId(R.id.quantity)));
        quantity.perform(replaceText("3"), closeSoftKeyboard());

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Add product
        ViewInteraction addToShoppingList = onView(allOf(withId(R.id.accion)));
        addToShoppingList.perform(click());

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Go to shoppingList
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open()); // Close Drawer

        // Start the screen of shoppinglist fragment
        onView(withId(R.id.nav_view))
                .perform(NavigationViewActions.navigateTo(R.id.nav_lista_compra));

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        RecyclerView recyclerViewShoppingList = mActivityTestRule.getActivity().findViewById(R.id.listShopping);
        int productShoppingListItems = recyclerViewShoppingList.getAdapter().getItemCount();

        if (productShoppingListItems > 0) {
            ViewInteraction productShListName= onView(
                    allOf(withId(R.id.nombre_producto), withText("Ajo Polvo"),
                            childAtPosition(
                                    childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                            3),
                                    0),
                            isDisplayed()));
            productShListName.check(matches(isDisplayed()));

            ViewInteraction productShListPrice= onView(
                    allOf(withId(R.id.precio_prod_lista), withText("18.0 euros/ud"),
                            childAtPosition(
                                    childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                            3),
                                    1),
                            isDisplayed()));
            productShListPrice.check(matches(isDisplayed()));

            ViewInteraction productShListQuantity= onView(
                    allOf(withId(R.id.cantidad_producto_lista), withText("x3 uds"),
                            childAtPosition(
                                    childAtPosition(
                                            IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                            0
                                    ),
                                    4),
                            isDisplayed()));
            productShListQuantity.check(matches(isDisplayed()));
        }

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        ViewInteraction makeOrderButton = onView(allOf(withId(R.id.realizarpedido), withText("Realizar pedido")));
        makeOrderButton.perform(click());

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Go to orderList
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open()); // Close Drawer

        // Start the screen of orders fragment
        onView(withId(R.id.nav_view))
                .perform(NavigationViewActions.navigateTo(R.id.nav_pedidos));

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        RecyclerView recyclerViewOrders = mActivityTestRule.getActivity().findViewById(R.id.order_list);
        int orderListItems = recyclerViewOrders.getAdapter().getItemCount();
        Assert.assertNotEquals(orderListItems, 0);

        if (orderListItems > 0) {
            ViewInteraction totalPriceOrder = onView(
                    allOf(withId(R.id.precio_total_pedido), withText("54.0 euros"),
                            childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                    2),
                            isDisplayed()));
            totalPriceOrder.check(matches(isDisplayed()));

            ViewInteraction deliveryAddress = onView(
                    allOf(withId(R.id.direccion_entrega), withText("Entrega: Avda. Alemania, 21, 4ºB"),
                            childAtPosition(
                                    childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                    1),
                            2),
                            isDisplayed()));
            deliveryAddress.check(matches(isDisplayed()));
        }

        //Delete user to prevent errors in following tests
        menu.perform(click());
        onView(withText("Borrar usuario")).perform(click());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
