package asee.giis.unex.vegenatnavigationdrawer.unitTest;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import asee.giis.unex.vegenatnavigationdrawer.LiveDataTestUtils;
import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Product;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.ProductDAO;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.VegenatDatabase;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ProductDAOUnitTest {

    private ProductDAO productDAO;
    private VegenatDatabase vegenatDatabase;

    @Rule
    public TestRule rule = new androidx.arch.core.executor.testing.InstantTaskExecutorRule();

    @Before
    public void createDatabase() {
        Context context = getInstrumentation().getContext();
        vegenatDatabase = Room.inMemoryDatabaseBuilder(context, VegenatDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        productDAO = vegenatDatabase.getProductDAO();
    }

    @Test
    public void bulkInsertAndListProductsTest() throws InterruptedException {
        Product p1 = new Product(10, "Patata", "Patata ecológica deshidratada", 15.6F, "www.example.com/patata");
        Product p2 = new Product(11, "Zanahoria", "Zanahoria ecológica", 13.3F, "www.example.com/zanahoria");

        List<Product> lProducts = new ArrayList<Product>();
        lProducts.add(p1);
        lProducts.add(p2);

        productDAO.bulkInsert(lProducts);

        LiveData<List<Product>> liveProductsList = productDAO.listAllProducts();

        List<Product> gotProducts = LiveDataTestUtils.getValue(liveProductsList);

        assertTrue(gotProducts.size() == 2);
        assertTrue( gotProducts.get(0).getId() == p1.getId());
    }

    @Test
    public void searchProductByIdTest() throws InterruptedException {
        Product p1 = new Product(13, "Lechuga", "Lechuga", 15.6F, "www.example.com/lechuga");
        Product p2 = new Product(14, "Puerro", "Puerro", 13.3F, "www.example.com/puerro");

        List<Product> lProducts = new ArrayList<Product>();
        lProducts.add(p1);
        lProducts.add(p2);

        productDAO.bulkInsert(lProducts);

        LiveData<Product> liveProduct1 = productDAO.searchById(13);
        Product returnProduct1 = LiveDataTestUtils.getValue(liveProduct1);
        LiveData<Product> liveProduct2 = productDAO.searchById(14);
        Product returnProduct2 = LiveDataTestUtils.getValue(liveProduct2);
        LiveData<Product> nullLiveProduct = productDAO.searchById(15);
        Product nullReturnProduct = LiveDataTestUtils.getValue(nullLiveProduct);

        assertNotNull(returnProduct1);
        assertNotNull(returnProduct2);
        assertNull(nullReturnProduct);
    }

    @Test
    public void deleteAllProductsTest() {
        Product p1 = new Product(11, "Cebolla", "Cebolla ecológica deshidratada", 15.6F, "www.example.com/cebolla");
        Product p2 = new Product(10, "Ajo", "Ajo ecológico deshidratado", 13.3F, "www.example.com/ajo");

        List<Product> lProducts = new ArrayList<Product>();
        lProducts.add(p1);
        lProducts.add(p2);

        productDAO.bulkInsert(lProducts);

        int rowsDeleted = productDAO.deleteAllProducts();

        //Devuelve 2 filas borradas porque acabamos de insertar dos productos
        assertTrue(rowsDeleted == 2);
    }

    @After
    public void closeDatabase() {
        vegenatDatabase.clearAllTables();
        vegenatDatabase.close();
    }
}
