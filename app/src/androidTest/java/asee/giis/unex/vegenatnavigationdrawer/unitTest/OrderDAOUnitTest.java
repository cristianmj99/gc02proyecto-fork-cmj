package asee.giis.unex.vegenatnavigationdrawer.unitTest;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;

import asee.giis.unex.vegenatnavigationdrawer.LiveDataTestUtils;
import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Order;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.OrderDAO;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.VegenatDatabase;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class OrderDAOUnitTest {

    private OrderDAO orderDAO;
    private VegenatDatabase vegenatDatabase;

    @Rule
    public TestRule rule = new androidx.arch.core.executor.testing.InstantTaskExecutorRule();

    @Before
    public void createDatabase() {
        Context context = getInstrumentation().getContext();
        vegenatDatabase = Room.inMemoryDatabaseBuilder(context, VegenatDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        orderDAO = vegenatDatabase.getOrderDAO();
    }

    @Test
    public void insertAndGetOrdersTest() throws InterruptedException {
        Order order1 = new Order(1, "30/11/2021", 35.4F, "Avda. Alemania, nº6, 4ºA");
        order1.setId_order(1);
        Order order2 = new Order(1, "02/12/2021", 13F, "Avda. Alemania, nº6, 4ºA");
        order2.setId_order(2);

        orderDAO.insertOrder(order1);
        orderDAO.insertOrder(order2);

        LiveData<List<Order>> liveOrderList = orderDAO.getOrdersByUser(1);
        List<Order> orderList = LiveDataTestUtils.getValue(liveOrderList);

        assertTrue(orderList.size() == 2);
        assertEquals(orderList.get(0).getId_order(), order1.getId_order());
        assertEquals(orderList.get(1).getId_order(), order2.getId_order());

        liveOrderList = orderDAO.getOrdersByUser(200);
        orderList = LiveDataTestUtils.getValue(liveOrderList);
        assertTrue(orderList.size() == 0);
    }

    @Test
    public void deleteOrdersByUserTest() throws InterruptedException {
        Order order1 = new Order(15, "30/11/2021", 35.4F, "Avda. Alemania, nº6, 4ºA");
        order1.setId_order(1);
        Order order2 = new Order(15, "02/12/2021", 13F, "Avda. Alemania, nº6, 4ºA");
        order2.setId_order(2);

        orderDAO.insertOrder(order1);
        orderDAO.insertOrder(order2);

        LiveData<List<Order>> liveOrderList = orderDAO.getOrdersByUser(15);
        List<Order> orderList = LiveDataTestUtils.getValue(liveOrderList);

        assertTrue(orderList.size() == 2);
        assertEquals(orderList.get(0).getId_order(), order1.getId_order());
        assertEquals(orderList.get(1).getId_order(), order2.getId_order());

        orderDAO.deleteOrdersByUser(15);
        liveOrderList = orderDAO.getOrdersByUser(15);
        orderList = LiveDataTestUtils.getValue(liveOrderList);

        assertTrue(orderList.size() == 0);
    }

    @After
    public void closeDatabase() {
        vegenatDatabase.clearAllTables();
        vegenatDatabase.close();
    }

}
