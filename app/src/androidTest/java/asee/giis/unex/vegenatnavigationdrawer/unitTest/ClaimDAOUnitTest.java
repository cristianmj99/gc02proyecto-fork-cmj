package asee.giis.unex.vegenatnavigationdrawer.unitTest;

import android.content.Context;

import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;

import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.Claim;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.ClaimDAO;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.VegenatDatabase;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ClaimDAOUnitTest {

    private ClaimDAO claimDAO;
    private VegenatDatabase vegenatDatabase;

    @Rule
    public TestRule rule = new androidx.arch.core.executor.testing.InstantTaskExecutorRule();

    @Before
    public void createDatabase() {
        Context context = getInstrumentation().getContext();
        vegenatDatabase = Room.inMemoryDatabaseBuilder(context, VegenatDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        claimDAO = vegenatDatabase.getClaimDAO();
    }

    @Test
    public void insertAndGetClaimsFromUser() {
        Claim claim1 = new Claim(1, 22);
        claim1.setId_claim(1);
        Claim claim2 = new Claim(2, 22);
        claim2.setId_claim(2);

        claimDAO.insertClaim(claim1);
        claimDAO.insertClaim(claim2);

        List<Claim> claimList = claimDAO.getClaimsFromUser(22);
        assertTrue(claimList.size() == 2);
        assertEquals(claimList.get(0).getId_claim(), claim1.getId_claim());
        assertEquals(claimList.get(1).getId_claim(), claim2.getId_claim());

        claimList = claimDAO.getClaimsFromUser(33);
        assertTrue(claimList.size() == 0);
    }

    @After
    public void closeDatabase() {
        vegenatDatabase.clearAllTables();
        vegenatDatabase.close();
    }
}
