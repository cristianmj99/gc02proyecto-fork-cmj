package asee.giis.unex.vegenatnavigationdrawer.unitTest;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;

import asee.giis.unex.vegenatnavigationdrawer.LiveDataTestUtils;
import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.ProductWithQuantity;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.ProductShoppingListDAO;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.VegenatDatabase;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ProductShoppingListDAOUnitTest {

    private ProductShoppingListDAO productShoppingListDAO;
    private VegenatDatabase vegenatDatabase;

    @Rule
    public TestRule rule = new androidx.arch.core.executor.testing.InstantTaskExecutorRule();

    @Before
    public void createDatabase() {
        Context context = getInstrumentation().getContext();
        vegenatDatabase = Room.inMemoryDatabaseBuilder(context, VegenatDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        productShoppingListDAO = vegenatDatabase.getProductShoppingListDAO();
    }

    @Test
    public void insertAndRecoverProductsShoppingListTest() throws InterruptedException {
        ProductWithQuantity pwq1 = new ProductWithQuantity(1, "Ajo", 3, 18.6F);
        pwq1.setIdItemLista(1);
        ProductWithQuantity pwq2 = new ProductWithQuantity(1, "Cebolla", 1, 3.9F);
        pwq2.setIdItemLista(2);

        productShoppingListDAO.insertProductShoppingList(pwq1);
        productShoppingListDAO.insertProductShoppingList(pwq2);

        LiveData<List<ProductWithQuantity>> pwqLiveList = productShoppingListDAO.recoverAllProductsShoppingList(1);
        List<ProductWithQuantity> pwqList = LiveDataTestUtils.getValue(pwqLiveList);

        assertTrue(pwqList.size() == 2);
        assertEquals(pwqList.get(0).getIdItemLista(), pwq1.getIdItemLista());
        assertEquals(pwqList.get(1).getIdItemLista(), pwq2.getIdItemLista());

        ProductWithQuantity pwq3 = new ProductWithQuantity(1, "Patata", 6, 30.0F);
        pwq3.setIdItemLista(3);
        productShoppingListDAO.insertProductShoppingList(pwq3);

        pwqList = productShoppingListDAO.recoverAllProductsToOrder(1);

        assertTrue(pwqList.size() == 3);
        assertEquals(pwqList.get(0).getIdItemLista(), pwq1.getIdItemLista());
        assertEquals(pwqList.get(1).getIdItemLista(), pwq2.getIdItemLista());
        assertEquals(pwqList.get(2).getIdItemLista(), pwq3.getIdItemLista());

        pwqList = productShoppingListDAO.recoverAllProductsToOrder(3);
        assertTrue(pwqList.size() == 0);
    }

    @Test
    public void deleteProductShoppingListTest() throws InterruptedException {
        ProductWithQuantity pwq1 = new ProductWithQuantity(5, "Zanahoria", 3, 18.6F);
        pwq1.setIdItemLista(1);
        ProductWithQuantity pwq2 = new ProductWithQuantity(5, "Tomate", 1, 3.9F);
        pwq2.setIdItemLista(2);

        productShoppingListDAO.insertProductShoppingList(pwq1);
        productShoppingListDAO.insertProductShoppingList(pwq2);

        LiveData<List<ProductWithQuantity>> pwqLiveList = productShoppingListDAO.recoverAllProductsShoppingList(5);
        List<ProductWithQuantity> pwqList = LiveDataTestUtils.getValue(pwqLiveList);
        assertTrue(pwqList.size() == 2);

        //Delete one, get and check
        productShoppingListDAO.deleteProductShoppingList(pwq1);
        pwqLiveList = productShoppingListDAO.recoverAllProductsShoppingList(5);
        pwqList = LiveDataTestUtils.getValue(pwqLiveList);
        assertTrue(pwqList.size() == 1);

        //Delete another, get and check
        productShoppingListDAO.deleteProductShoppingList(pwq2);
        pwqLiveList = productShoppingListDAO.recoverAllProductsShoppingList(5);
        pwqList = LiveDataTestUtils.getValue(pwqLiveList);
        assertTrue(pwqList.size() == 0);
    }

    @Test
    public void deleteAllProductsShoppingListTest() throws InterruptedException {
        ProductWithQuantity pwq1 = new ProductWithQuantity(7, "Melón", 3, 18.6F);
        pwq1.setIdItemLista(1);
        ProductWithQuantity pwq2 = new ProductWithQuantity(7, "Sandía", 1, 3.9F);
        pwq2.setIdItemLista(2);

        productShoppingListDAO.insertProductShoppingList(pwq1);
        productShoppingListDAO.insertProductShoppingList(pwq2);

        LiveData<List<ProductWithQuantity>> pwqLiveList = productShoppingListDAO.recoverAllProductsShoppingList(7);
        List<ProductWithQuantity> pwqList = LiveDataTestUtils.getValue(pwqLiveList);
        assertTrue(pwqList.size() == 2);

        //Delete all
        productShoppingListDAO.deleteAllProductsShoppingList(7);
        pwqLiveList = productShoppingListDAO.recoverAllProductsShoppingList(7);
        pwqList = LiveDataTestUtils.getValue(pwqLiveList);
        assertTrue(pwqList.size() == 0);
    }

    @After
    public void closeDatabase() {
        vegenatDatabase.clearAllTables();
        vegenatDatabase.close();
    }

}
