package asee.giis.unex.vegenatnavigationdrawer.uiTest;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.*;
import static androidx.test.espresso.Espresso.onView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import asee.giis.unex.vegenatnavigationdrawer.MainActivity;
import asee.giis.unex.vegenatnavigationdrawer.R;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ViewProductsSetUITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public  void viewProductsSetTest() {
        // Open Drawer to click on navigation.
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))) // Left Drawer should be closed.
                .perform(DrawerActions.open()); // Open Drawer

        // Start the screen of products fragment
        onView(withId(R.id.nav_view))
                .perform(NavigationViewActions.navigateTo(R.id.nav_productos));

        //Delay to show products
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Test clicking in some element
        ViewInteraction recyclerViewInterac = onView(
                allOf(withId(R.id.product_list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)));
        recyclerViewInterac.perform(actionOnItemAtPosition(0, click()));

        //Go back to RecyclerView
        Espresso.pressBack();

        RecyclerView recyclerView = mActivityTestRule.getActivity().findViewById(R.id.product_list);
        int productItems = recyclerView.getAdapter().getItemCount();

        Assert.assertNotEquals(productItems, 0);
        // check that the number of RecyclerView elements is greater than 0
        if (productItems > 0){
            ViewInteraction textView1 = onView(
                    allOf(withId(R.id.nombre_producto), withText("Ajo copos"),
                            childAtPosition(
                                    childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                            1),
                                    0),
                            isDisplayed()));
            textView1.check(matches(isDisplayed()));

            ViewInteraction textView2 = onView(
                    allOf(withId(R.id.nombre_producto), withText("Cebolla Eco"),
                            childAtPosition(
                                    childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                            1),
                                    0),
                            isDisplayed()));
            textView2.check(matches(isDisplayed()));

            ViewInteraction textView3 = onView(
                    allOf(withId(R.id.precio_prod_lista), withText("20.5 euros"),
                            childAtPosition(
                                    childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                            1),
                                    1),
                            isDisplayed()));
            textView3.check(matches(isDisplayed()));

            ViewInteraction textView4 = onView(
                    allOf(withId(R.id.precio_prod_lista), withText("40.0 euros"),
                            childAtPosition(
                                    childAtPosition(
                                            childAtPosition(
                                                    IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                    0
                                            ),
                                            1),
                                    1),
                            isDisplayed()));
            textView3.check(matches(isDisplayed()));
        }


    }


    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

}
