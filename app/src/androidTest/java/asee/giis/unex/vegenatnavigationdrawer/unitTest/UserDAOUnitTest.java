package asee.giis.unex.vegenatnavigationdrawer.unitTest;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import asee.giis.unex.vegenatnavigationdrawer.LiveDataTestUtils;
import asee.giis.unex.vegenatnavigationdrawer.repository.model.local.User;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.UserDAO;
import asee.giis.unex.vegenatnavigationdrawer.roomdb.VegenatDatabase;

@RunWith(AndroidJUnit4.class)

public class UserDAOUnitTest {

    private UserDAO userDAO;
    private VegenatDatabase vegenatDatabase;
    private long iduser1, iduser2;

    @Rule
    public TestRule testRule = new InstantTaskExecutorRule();


    @Before
    public void createDatabase() {
        Context context = getInstrumentation().getContext();
        vegenatDatabase = Room.inMemoryDatabaseBuilder(context, VegenatDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        userDAO = vegenatDatabase.getUserDAO();
    }

    //En este test sólo se comprueba si se inserta bien o no, los diferentes casos de inserciones
    //username en uso, email en uso, password que no cumple requisitos, etc se comprueba en el Activity (UI Test)
    @Test
    public void insertUserTest() throws Exception {
        User user1 = new User("CristianMJ06091999", "cristianmj6999@gmail.com",
                "Hola1234!", "Avda. Alemania, 23, 3º D");
        User user2 = new User("CristianRR1999", "cristianroman99@gmail.com",
                "12345!Hola", "C/ Gil Cordero, 11, 4º D");

        iduser1 = userDAO.insertUser(user1);
        iduser2 = userDAO.insertUser(user2);

        assertTrue(iduser1 > 0);
        assertTrue(iduser2 > 0);

        assertNotEquals(iduser1, iduser2);
    }

    @Test
    public void editUserTest() throws Exception {
        User user1 = new User("CristianMJ99", "cristian@gmail.com",
                "Hola1234!", "Avda. Alemania, 23, 3º D");
        User user2 = new User("CristianRR99", "cristianroman1999@gmail.com",
                "12345!Hola", "C/ Gil Cordero, 11, 4º D");

        long iduser1test = userDAO.insertUser(user1);
        long iduser2test = userDAO.insertUser(user2);
        user1.setId(iduser1test);
        user2.setId(iduser2test);

        assertTrue(iduser1test > 0);
        assertTrue(iduser2test > 0);
        assertNotEquals(iduser1test, iduser2test);

        user1.setPassword("Hola12345!!");
        user2.setAddress("C/ Argentina 3, 4º D");

        int updateRows1 = userDAO.update(user1);
        int updateRows2 = userDAO.update(user2);

        assertTrue(updateRows1 == 1);
        assertTrue(updateRows2 == 1);
    }

    @Test
    public void deleteUserTest() {
        User user1 = new User("Cristian0609", "cristian@hotmail.com",
                "Hola1234!", "Avda. Alemania, 23, 3º D");
        User user2 = new User("CristianRR9", "cristianroman199@gmail.com",
                "12345!Hola", "C/ Gil Cordero, 11, 4º D");

        long iduser1test = userDAO.insertUser(user1);
        long iduser2test = userDAO.insertUser(user2);

        user1.setId(iduser1test);
        user2.setId(iduser2test);

        assertTrue(iduser1test > 0);
        assertTrue(iduser2test > 0);
        assertNotEquals(iduser1test, iduser2test);

        userDAO.deleteUser(iduser1test);
        userDAO.deleteUser(iduser2test);

        User returnUser1 = userDAO.searchUserByName(user1.getUsername());
        User returnUser2 = userDAO.searchUserByName(user2.getUsername());

        assertNull(returnUser1);
        assertNull(returnUser2);
    }

    @Test
    public void searchByUsernameTest() {
        User user1 = new User("CristianMJ9", "cristianmj9999999@hotmail.com",
                              "Hola1234!", "Avda. Alemania, 23, 3º D");
        long iduser1test = userDAO.insertUser(user1);
        user1.setId(iduser1test);

        assertTrue(iduser1test > 0);

        User returnUser = userDAO.searchUserByName(user1.getUsername());
        User returnUserNull = userDAO.searchUserByEmail(user1.getUsername() + "hhhhhhh");

        assertNull(returnUserNull);
        assertNotNull(returnUser);
        assertEquals(returnUser.getId(), user1.getId());
    }

    @Test
    public void searchByEmailTest() {
        User user1 = new User("CristianMJ908", "cristianmj999999999@hotmail.com",
                "Hola1234!", "Avda. Alemania, 23, 3º D");
        long iduser1test = userDAO.insertUser(user1);
        user1.setId(iduser1test);

        assertTrue(iduser1test > 0);

        User returnUser = userDAO.searchUserByEmail(user1.getEmail());
        User returnUserNull = userDAO.searchUserByEmail("hhh" + user1.getEmail());

        assertNull(returnUserNull);
        assertNotNull(returnUser);
        assertEquals(returnUser.getId(), user1.getId());
    }

    //Pendiente
    @Test
    public void searchUserById() throws InterruptedException {
        User user1 = new User("CristianMJ907", "cristianmj99990609@hotmail.com",
                              "Hola1234!", "Avda. Alemania, 23, 3º D");
        long iduser1test = userDAO.insertUser(user1);
        user1.setId(iduser1test);
        assertTrue(iduser1test > 0);

        LiveData<User> liveUser = userDAO.searchUserById(iduser1test);
        User returnUser = LiveDataTestUtils.getValue(liveUser);

        assertNotNull(returnUser);
        assertEquals(returnUser.getId(), user1.getId());
    }

    @After
    public void closeDatabase() {
        vegenatDatabase.clearAllTables();
        vegenatDatabase.close();
    }
}
