package asee.giis.unex.vegenatnavigationdrawer.uiTest;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import asee.giis.unex.vegenatnavigationdrawer.MainActivity;
import asee.giis.unex.vegenatnavigationdrawer.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ViewUserUITest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void viewUserTest() {
        //Open user menu
        ViewInteraction menu = onView(allOf(withId(R.id.empty)));
        menu.perform(click());

        onView(withText("Registrar usuario")).perform(click());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatEditText1 = onView(allOf(withId(R.id.username)));
        appCompatEditText1.perform(replaceText("CristianMJ6999"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(allOf(withId(R.id.email)));
        appCompatEditText2.perform(replaceText("cristianmj123456@gmail.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(allOf(withId(R.id.address)));
        appCompatEditText3.perform(replaceText("Avda. Alemania, 21, 4ºB"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(allOf(withId(R.id.password)));
        appCompatEditText4.perform(replaceText("Hola12345!"), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(allOf(withId(R.id.confirmpassword)));
        appCompatEditText5.perform(replaceText("Hola12345!"), closeSoftKeyboard());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Try to register with incorrect password (don't match regexp)
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.registrarse), withText("Registrar usuario")));
        appCompatButton.perform(click());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //View user info
        menu.perform(click());
        onView(withText("Editar usuario")).perform(click());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction username = onView(
                allOf(withId(R.id.editusername), withText("CristianMJ6999"), isDisplayed()));
        username.check(matches(isDisplayed()));

        ViewInteraction email = onView(
                allOf(withId(R.id.editemail), withText("cristianmj123456@gmail.com"), isDisplayed()));
        email.check(matches(isDisplayed()));

        ViewInteraction address = onView(
                allOf(withId(R.id.editaddress), withText("Avda. Alemania, 21, 4ºB"), isDisplayed()));
        address.check(matches(isDisplayed()));


        Espresso.pressBack();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Delete user
        menu.perform(click());
        onView(withText("Borrar usuario")).perform(click());
    }
}
